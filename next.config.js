const webpack = require('webpack')
const path = require('path');
const dotEnv = require('dotenv-webpack');

module.exports = {
  webpack: config => {
    config.module.rules.push(
      {
        test: /\.scss$/,
        loader: 'babel-loader!raw-loader!sass-loader'
      }
    )

    config.plugins = config.plugins || [];

    config.plugins.push(
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      }),
    )

    return config;
  }
};