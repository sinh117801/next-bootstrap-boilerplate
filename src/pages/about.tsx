import React from 'react';
import Head from 'next/head';
import Nav from '../components/nav';
import Layout from '../components/layout';

const Home = () => {
  return (
    <Layout>
      <div>
        <Head>
          <title>Home</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <Nav />

        <div className="hero">
          <h1 className="title example">{process.env.TITLE}</h1>
          <p className="description">
            To get started, edit <code>pages/index.js</code> and save to reload.
          </p>
        </div>
      </div>
    </Layout>
  );
};

export default Home;
