import React, { Fragment } from 'react';
import Head from 'next/head';
import Layout from '../components/layout';
import { useRouter } from 'next/router';
import FacebookSEO from '../components/seo/FacebookSEO';

const Home = () => {
  const router = useRouter();
  return (
    <Fragment>
      <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
        <FacebookSEO
          url={`${process.env.BASE_URL}${router.pathname}`}
          title="Home"
          image=""
          description="hello"
        />
      </Head>
      <Layout>
        <div className="hero">
          <h1 className="title">{process.env.TITLE}</h1>
          <p className="description">
            To get started, edit <code>pages/index.js</code> and save to reload.
          </p>
        </div>
      </Layout>
    </Fragment>
  );
};

export default Home;
