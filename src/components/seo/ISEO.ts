interface ISEO {
  url: string;
  title: string;
  image: string;
  description: string;
}

export interface IFacebookSEO extends ISEO {
  type?: string;
  siteName?: string;
  locale?: string;
  imageWidth?: string;
  imageHeight?: string;
}

export interface ITwitterSEO extends ISEO {
  card?: string;
  site?: string;
  creator?: string;
}
