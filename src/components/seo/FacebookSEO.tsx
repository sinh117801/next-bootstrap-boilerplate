import React from 'react';
import { IFacebookSEO } from './ISEO';
import Head from 'next/head';


export interface FacebookSEOProps {
  url: string;
  title: string;
  image: string;
  description: string;
  type?: string;
  siteName?: string;
  locale?: string;
  imageWidth?: string;
  imageHeight?: string;
}

const FacebookSEO: React.FC<FacebookSEOProps> = ({
  url,
  title,
  image,
  description,
  type = 'website',
  siteName = 'Lazzi Buy',
  locale = 'en_US',
  imageWidth = '1200',
  imageHeight = '630'
}) => {
  return (
    <Head>
      <meta property="og:type" content={type} />
      <meta property="og:url" content={url} />
      <meta property="og:title" content={title} />
      <meta property="og:image" content={image} />
      <meta property="og:description" content={description} />
      <meta property="og:site_name" content={siteName} />
      <meta property="og:locale" content={locale || 'en_US'} />
      <meta property="og:image:width" content={imageWidth} />
      <meta property="og:image:height" content={imageHeight} />
    </Head>
  );
};

export default FacebookSEO;
